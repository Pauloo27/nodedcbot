const fs = require('fs')
const Logger = require('./logger')

if (!fs.existsSync('../config.js')) {
  fs.copyFileSync('../config.js.default', '../config.js')
  throw new Error('Config not found, createing a new one.')
}

const { prefix, token, presence } = require('../config')
const { Client } = require('discord.js')
const client = new Client()

module.exports.client = client

const logger = new Logger()

module.exports.logger = logger

const { CommandManager } = require('./command/command_api')

CommandManager.prefix = prefix

var commandCount = 0
var categoryCount = 0

function reloadCommands() {
  CommandManager.clear()
  fs.readdirSync('./src/commands').forEach((folder) => {
    if (fs.lstatSync('./src/commands/' + folder).isDirectory()) {
      const category = require('./commands/' + folder + '/category.js')
      categoryCount++
      fs.readdirSync('./src/commands/' + folder).forEach(file => {
        if (file.endsWith('.js') && file !== 'category.js') {
          delete require.cache[require.resolve('./commands/' + folder + '/' + file)]
          const command = require('./commands/' + folder + '/' + file)
          command.category = category
          CommandManager.register(command)

          commandCount++
        }
      })
    }
  })
}

module.exports.reloadCommands = reloadCommands

process.on('SIGINT', function () {
  client.destroy()
  process.exit()
});

reloadCommands()
client.login(token)

client.on('ready', () => {
  logger.info(`Logged in as ${client.user.tag}!`)
  logger.info(`Loaded ${commandCount} commands in ${categoryCount} categories.`)
  client.user.setPresence(presence)
});

client.on('message', msg => {
  if (msg.author.id == client.user.id) return;

  CommandManager.run(msg)
});

const stdin = process.openStdin()

stdin.addListener("data", function (input) {
  input = input.toString().trim()

  if (input.length == 0)
    return;

  if (!input.startsWith(prefix))
    input = prefix + input

  const msg = {
    reply: function (text) { logger.debug(text) },
    content: input,
    author: client.user,
    channel: {
      send: function (embed) { logger.debug(embed) }
    }
  }
  logger.debug(' > running ' + input)
  logger.debug()
  CommandManager.run(msg)
  logger.debug()
  logger.debug(' ======= ')
});

