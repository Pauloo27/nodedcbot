const { Command, Parameter, DefaultTypes, DefaultPermissions } = require('../../command/command_api')

class BanCommand extends Command {
  constructor() {
    super(
       /* nome */
      'ban',
      /* desc */
      'Bane um membro', 
      /* exemplos */
      ['@user foto inadequada', '@user spam', '@user'],
      [ 
        /* ==lista parametros== */
        /* Parameter(   nome,         desc,                 tipo,            obrigatório?) */
        new Parameter('usuário', 'usuário para ser banido', DefaultTypes.member, true),
        /* Parameter(   nome,         desc,                 tipo,            obrigatório?) */
        new Parameter('motivo', 'motivo do banimento', DefaultTypes.text, false),
      ],
      /* (aliases) nomes alternativos (nenhum pra esse kkk) */
      [], 
      /* permissão */
      new DefaultPermissions.ByDiscordPermission('banir usuário', 'BAN_MEMBERS') 
    )
  }

  execute(context, member, reason) { 
    if (reason == null) reason = 'Nenhum motivo informado'

    if (member.bannable) {
      member.ban(reason)
      context.success('Usuário banido!')
    } else {
      context.error('Um erro ocorreu! Talvez eu não tenha permissão para banir esse usuário.')
    }
  }
}

module.exports = new BanCommand()
