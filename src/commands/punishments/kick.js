const { Command, Parameter, DefaultTypes, DefaultPermissions } = require('../../command/command_api')

class BanCommand extends Command {
  constructor() {
    super(
      'kick',
      'Expulsa um membro',
      ['@user foto inadequada', '@user spam', '@user'],
      [
        new Parameter('usuário', 'usuário para ser banido', DefaultTypes.member, true),
        new Parameter('motivo', 'motivo da expulsão', DefaultTypes.text, false),
      ],
      [],
      new DefaultPermissions.ByDiscordPermission('expulsar usuário', 'KICK_MEMBERS')
    )
  }

  execute(context, member, reason) {
    if (reason == null) reason = 'Nenhum motivo informado'

    if (member.kickable) {
      member.kick(reason)
      context.success('Usuário expulso!')
    } else {
      context.error('Um erro ocorreu! Talvez eu não tenha permissão para kickar esse usuário.')
    }
  }
}

module.exports = new BanCommand()
