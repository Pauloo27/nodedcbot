const { Category } = require('../../command/command_api')

module.exports = new Category(
  'punições',
  'comandos para aplicar punições nos membros'
)
