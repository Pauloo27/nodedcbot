const { Command, DefaultTypes, Parameter } = require('../../command/command_api')

class AvatarCommand extends Command {
  constructor() {
    super(
      'avatar',
      'Pega foto de um usuário ou de si mesmo',
      ['', '@usuário'],
      [
        new Parameter('usuário', 'usuário para pegar a foto de perfil', DefaultTypes.user, false),
      ],
      ['ppic', 'pp', 'pic', 'foto']
    )
  }

  execute(context, user) {
    if (user == null)
      user = context.msg.author

    context.success(user.avatarURL)
  }
}

module.exports = new AvatarCommand()
