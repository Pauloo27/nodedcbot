const { Command, DefaultTypes, Parameter, DefaultPermissions } = require('../../command/command_api')
const { client, config } = require('../../app')

class PresenceCommand extends Command {
  constructor() {
    super(
      'presence',
      'Mude a presença do Bot',
      [
        "PLAYING . Minecraft",
        "STREAMING https://twitch.tv/Pauloo2709 Minecraft",
        "LISTENING . Sweden",
        "WATCHING . Netflix (Minecraft Story Mode)"
      ],
      [
        new Parameter('status', 'PLAYING / STREAMING / LISTENING / WATCHING',
          new DefaultTypes.enum('PLAYING', 'STREAMING', 'LISTENING', 'WATCHING'), true
        ),
        new Parameter('url', 'https://twitch.tv/Pauloo2709', DefaultTypes.string, true),
        new Parameter('nome', 'Minecraft', DefaultTypes.text, true)
      ],
      ['jogando', 'presença', 'status'],
      new DefaultPermissions.Owner()
    )
  }

  execute(context, status, url, name) {
    const presence =
    {
      game: {
        name: name,
        type: status.toUpperCase()
      },
      status: 'online'
    }

    if(url != '.') presence.game.url = url

    try {
      client.user.setPresence(presence)
      // TODO: Save in the config
      context.success(':ok_hand:')
    } catch(e) {
      context.error(e)
    }
  }
}

module.exports = new PresenceCommand()
