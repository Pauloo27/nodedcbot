const { Command, DefaultTypes, Parameter } = require('../../command/command_api')

const Canvas = require('canvas')
const Discord = require('discord.js')

class BolsonaroCommand extends Command {
    constructor() {
        super(
            'bolsonaro',
            'Pega foto de um usuário e coloca o bolsonaro junto',
            ['link'],
            [
                new Parameter('link', 'link para uma imagem', DefaultTypes.string, false)
            ],
            ['bolso', 'presida', 'tv'],
            null,
            function (context) {
                return context.args || context.msg.attachments.size != 0
            }
        )
    }

    async execute(context, link) {
        if (link == null)
            link = context.msg.attachments.first().url

        const canvas = Canvas.createCanvas(400, 228)
        const ctx = canvas.getContext('2d')

        const bolsonaro = await Canvas.loadImage('./assets/Bolsonaro.png')
        const image = await Canvas.loadImage(link)
        // const avatar = await Canvas.loadImage(user.displayAvatarURL)

        ctx.drawImage(image, 107, 9, 275, 157)
        ctx.drawImage(bolsonaro, 0, 0, canvas.width, canvas.height)

        const attachment = new Discord.Attachment(canvas.toBuffer(), 'bolsonaro.png')

        context.send(context.author.toString(), attachment)
    }
}

module.exports = new BolsonaroCommand()
