const { Command, DefaultTypes, Parameter } = require('../../command/command_api')

class SayCommand extends Command {
  constructor() {
    super(
      'dizer',
      'Dizer algo',
      'Olá mundo',
      [
        new Parameter('texto', 'texto a ser falado', DefaultTypes.text, true),
      ],
      ['tell', 'msg', 'message', 'say']
    )
  }
  
  execute(context, text) { context.success(text) }

}

module.exports = new SayCommand()
