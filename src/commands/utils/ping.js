const { Command } = require('../../command/command_api')

class PingCommand extends Command {
  constructor() {
    super(
      'ping',
      'Responde pong!',
      ''
    )
  }

  execute(context) { context.reply(':ping_pong: Pong!!1!') }
}

module.exports = new PingCommand()
