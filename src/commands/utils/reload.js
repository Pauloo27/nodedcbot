const { Command, DefaultPermissions } = require('../../command/command_api')
const app = require('../../app')

class ReloadCommand extends Command {
  constructor() {
    super(
      'reload',
      'Recarregue os comandos',
      '',
      [],
      [],
      new DefaultPermissions.Owner()
    )
  }

  execute(context) {
    app.reloadCommands()
    context.success('Comandos recarregados!')
  }
}

module.exports = new ReloadCommand()
