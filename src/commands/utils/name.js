const { Command, DefaultTypes, Parameter } = require('../../command/command_api')

const month = [
  'Tomboy',
  'Maid',
  'Police',
  'MILF',
  'MagicalGirl',
  'Prego',
  'Futa',
  'Loli',
  'Cat Girl',
  'Nurse',
  'Teacher',
  'Office Lady'
]

const day = [
  'Twins',
  'Muscle',
  'Harem',
  'Straight Shota',
  'Monster Girl',
  'Tan Lines',
  'Prostitution',
  'Gyaru',
  'Plugsuit',
  'Trap',
  'Demon',
  'Femdom',
  'Mind Control',
  'Paizuri',
  'Sleeping',
  'Cheating',
  'Elf',
  '3-way',
  'Micro - Bikini',
  'BBW',
  'Tsundere',
  'Pantyhose',
  'Apron only',
  'Sister',
  'Blackmail',
  'Cosplay',
  'Yandere',
  '4-way',
  'Dark Skin',
  'Exhbitionism',
  'Alien'
]

const year = [
  'Netori',
  'Incest',
  'Yuri',
  'Netorare',
  'Tentacles',
  'Bakunyuu',
  'Vanilla',
  'Chikan',
  'Pettanko',
  'Reverse Rape'
]

class NameCommand extends Command {
  constructor() {
    super(
      'nome',
      'Define o nome usando o padrão do servidor',
      '27/09/2002',
      [
        new Parameter('nascimento', 'data de nascimento usando o padrão dd/mm/yyyy', DefaultTypes.text, true,
          function (arg, context) {
            if (!arg.match(/[0-9]{2}\/[0-9]{2}\/[0-9]{4}/g))
              return false

            const date = arg.split('/')
            return day.length >= date[0] && month.length >= date[1] && year.length >= date[2].slice(-1)
          }
        )
      ],
      ['name']
    )
  }

  execute(context, arg) {
    const date = arg.split('/')
    const name = `${day[date[0] - 1]} ${month[date[1] - 1]} ${year[date[2].slice(-1)]}`
    // context.member.setNickname(name)
    context.success(name)
  }

}

module.exports = new NameCommand()
