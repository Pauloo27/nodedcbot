const { Command, DefaultTypes, Parameter, DefaultPermissions } = require('../../command/command_api')

class EvalCommand extends Command {
  constructor() {
    super(
      'eval',
      'Rode código JS',
      "console.log('hello')",
      [
        new Parameter('código', 'código para rodar', DefaultTypes.text, true),
      ],
      [],
      new DefaultPermissions.Owner()
    )
  }

  execute(context, code) {
    try {
      const result = eval(code)
      context.success(result)
    } catch(e) {
      context.error(e)
    }
  }
}

module.exports = new EvalCommand()
