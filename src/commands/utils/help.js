const { Command, DefaultTypes, Parameter } = require('../../command/command_api')
const { copyDict } = require('../../utils')

class HelpCommand extends Command {
  constructor() {
    super(
      'ajuda',
      'Ajuda sobre os comandos do Bot',
      ['', 'somar'],
      [
        new Parameter('comando', 'comando para mostrar informações', DefaultTypes.string, false,
          function (arg, context) {
            return arg.toLowerCase() in context.commandsMap
          }),
      ],
      ['h', 'hp', 'help']
    )
  }

  execute(context, commandName) {
    if (commandName == null) {
      let commands = 'Meu prefixo é ' + context.prefix + '\n\n'
      Object.keys(context.commandsMap).forEach(commandName => {
        var command = context.commandsMap[commandName]

        // to ignore aliases
        if (command.name == commandName) {
          commands += `**${command.name}:** ${command.description}\n`
        }
      })
      context.success(commands.trim())
    } else {
      var localContext = copyDict(context)
      localContext.commandName = commandName.toLowerCase()
      context.msg.channel.send(context.commandsMap[commandName.toLowerCase()].usageEmbed(localContext, ':question: Ajuda'))
    }
  }
}

module.exports = new HelpCommand()
