const { Command, Parameter, DefaultTypes } = require('../../command/command_api')
const sum = require('./utils/pa_utils').sum

class PaSumCommand extends Command {
  constructor() {
    super(
      'somapa',
      'Soma dos elementos de uma PA',
      '1 2 10',
      [
        new Parameter(
          'a1', 'primeiro elemento da PA', DefaultTypes.float, true
        ),
        new Parameter(
          'r', 'razão da PA', DefaultTypes.float, true
        ),
        new Parameter(
          'n', 'índice do elemento da PA para ser calculado', DefaultTypes.float, true
        )      ],
      ['pas', 'pasoma', 'sumpa', 'pasum']
    )
  }

  execute(context, a1, r, n) {
    const s = sum(a1, r, n)
    context.success(`A soma de ${n} elementos da PA com r = ${r}, a1 = ${a1} é: **${s}**.`)
  }
}

module.exports = new PaSumCommand()
