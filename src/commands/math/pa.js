const { Command, Parameter, DefaultTypes } = require('../../command/command_api')
const get = require('./utils/pa_utils').get

class PACommand extends Command {
  constructor() {
    super(
      'pa',
      'Calcula o valor do elemento n elemento de uma determinada PA',
      '1 2 10',
      [
        new Parameter(
          'a1', 'primeiro elemento da PA', DefaultTypes.float, true
        ),
        new Parameter(
          'r', 'razão da PA', DefaultTypes.float, true
        ),
        new Parameter(
          'n', 'índice do elemento da PA para ser calculado', DefaultTypes.float, true
        )
      ]
    )
  }

  execute(context, a1, r, n) {
    const an = get(a1, r, n)

    context.success(`Elemento ${n} da PA com r = ${r}, a1 = ${a1} é: **${an}**.`)
  }

}

module.exports = new PACommand()
