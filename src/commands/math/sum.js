const { Command, DefaultTypes, Parameter } = require('../../command/command_api')

class SumCommand extends Command {
  constructor() {
    super(
      'somar',
      'Some dois números',
      '2 3',
      [
        new Parameter('n1', 'primeiro número', DefaultTypes.float, true),
        new Parameter('n2', 'segundo número', DefaultTypes.float, true)
      ],
      ['sum']
    )
  }

  execute(context, n1, n2) { context.success(`${n1} + ${n2} = ${n1 + n2}.`) }

}

module.exports = new SumCommand()
