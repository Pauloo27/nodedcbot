const { Command, Parameter } = require('../../command/command_api')
const PointType = require('./utils/point_type')

class IsAlignCommand extends Command {
  constructor() {
    super(
      'alinhado',
      'Verifique se 3 pontos são colineares',
      '2,2 3,-1 3,2',
      [
        new Parameter('p1', 'primeiro ponto', PointType, true),
        new Parameter('p2', 'segundo ponto', PointType, true),
        new Parameter('p3', 'terceiro ponto', PointType, true)
      ],
      ['colinear']
    )
  }

  execute(context, p1, p2, p3) {
    const result = p1.det(p2, p3)
    
    const right = result[0]
    const left = result[1]
    const det = result[2]
    const isAlign = result[3]

    if (isAlign) {
      context.success('Alinhado!')
    } else {
      context.error(`Não alinhado, determinante vale ${det}.`)
    }
    context.reply(`${p1.x * p2.y} | ${p1.y * p3.x} | ${p2.x * p3.y} = ${right}`)
    context.reply(`${p1.y * p2.x * -1} | ${p1.x * p3.y * -1} | ${p2.y * p3.x * -1} = ${left}`)
  }

}

module.exports = new IsAlignCommand()
