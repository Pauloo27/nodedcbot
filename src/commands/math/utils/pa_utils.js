// a(n) = a1 + (n - 1) * r
module.exports.get = function(a1, r, n) {
  return a1 + (n - 1) * r
}

// Sa(n) = a1 + an * n / 2
module.exports.sum = function(a1, r, n) {
  return (a1 + module.exports.get(a1, r, n) * n) / 2
}
