const Type = require('../../../command/type')
const Point = require('./point')

class PointType extends Type {
  constructor() {
    super('ponto', 'x,y')
  }

  validator(arg, context) {
    const match = arg.match(/,/g)
    if (!match || match.length != 1) return false
    const c = arg.split(',')
    let valid = true
    c.forEach(n => {
      if (valid && isNaN(n)) {
        valid = false
      }
    })
    return valid
  }

  parser(arg, context) {
    const p = arg.split(',')
    return new Point(p[0], p[1])
  }
}

module.exports = new PointType();
