class Point {
  constructor(x, y) {
    this.x = parseFloat(x)
    this.y = parseFloat(y)
  }

  det(p2, p3) {
    const p1 = this
    const right = p1.x * p2.y + p1.y * p3.x + p2.x * p3.y
    const left = p1.y * p2.x * -1 + p1.x * p3.y * -1 + p2.y * p3.x * -1

    const det = right + left

    return [right, left, det, det === 0]
  }

  isAlign(p2, p3) {
    const p1 = this
    const right = p1.x * p2.y + p1.y * p3.x + p2.x * p3.y
    const left = p1.y * p2.x * -1 + p1.x * p3.y * -1 + p2.y * p3.x * -1

    const det = right + left
    return det === 0
  }
}

module.exports = Point
