const { Command, Parameter, DefaultTypes } = require('../../command/command_api')
const Canvas = require('canvas')
const Discord = require('discord.js')
const Point = require('./utils/point')
const PointType = require('./utils/point_type')

// colors and stuffs
const linesSize = 3
const bgColor = '#222f3e'
const textColor = '#c8d6e5'
const textSize = '20'
const textFont = 'Arial'
const baseColor = '#ee5253'
const lineColor = '#ff6b6b'
const dotColor = '#00d2d3'
const dotSize = 4
const symmetricalAxis = true

// the length of the unit in pixels
const unitInPixels = 40

// fixes (?) the coord of the line width 
function removeWidth(ctx, c) {
  if (c > 0) return c - ctx.lineWidth / 2
  else return c + ctx.lineWidth / 2
}

// returns the x coord relative to the center, instead of the left top
function convertX(ctx, x) {
  return removeWidth(ctx, ctx.canvas.width / 2 + x * unitInPixels)
}

// returns the y coord relative to the center, instead of the left top
// also invert the coord fix the direction
function convertY(ctx, y) {
  return removeWidth(ctx, ctx.canvas.height / 2 + (y * unitInPixels * -1))
}

class LineCommand extends Command {
  constructor() {
    super(
      'linha',
      'traça uma reta entre os pontos',
      '2.5,3 5,-2',
      [
        new Parameter('x,y', 'pontos', PointType, true, null, true),
      ],
      ['reta', 'line', 'l']
    )
  }

  execute(context, ...points) {
    const coordsX = []
    const coordsY = []
    points.forEach(p => {
      if (p.x > 0) coordsX.push(p.x)
      else coordsX.push(p.x * -1)

      if (p.y > 0) coordsY.push(p.y)
      else coordsY.push(p.y * -1)
    })

    // quadrant size (min = 4)
    let quadrantSizeX = Math.max(4, ...coordsX)
    let quadrantSizeY = Math.max(4, ...coordsY)

    // adds the border to let the unit count text visible
    quadrantSizeX++
    quadrantSizeY++

    if (symmetricalAxis) {
      quadrantSizeX = Math.max(quadrantSizeX, quadrantSizeY)
      quadrantSizeY = quadrantSizeX
    }

    // setups and stuffs
    const canvas = Canvas.createCanvas(2 * quadrantSizeX * unitInPixels, 2 * quadrantSizeY * unitInPixels)
    const ctx = canvas.getContext('2d')
    ctx.lineWidth = linesSize

    // sets the background
    ctx.fillStyle = bgColor
    ctx.fillRect(0, 0, canvas.width, canvas.height)

    // axis setup and stuffs
    ctx.strokeStyle = baseColor

    // draws x axis
    ctx.beginPath()
    // ps: use 1 unit as border
    ctx.lineTo(unitInPixels, removeWidth(ctx, canvas.height / 2))
    ctx.lineTo(canvas.width - unitInPixels, removeWidth(ctx, canvas.height / 2))
    ctx.stroke()

    // draws y axis
    ctx.beginPath()
    // ps: use 1 unit as border as well
    ctx.lineTo(removeWidth(ctx, canvas.width / 2), unitInPixels)
    ctx.lineTo(removeWidth(ctx, canvas.width / 2), canvas.height - unitInPixels)
    ctx.stroke()

    // draws the unit markers in the x axis
    Array.from(Array(canvas.width / unitInPixels / 2).keys()).forEach(i => {
      if (i != 0) {
        const draw = function (x) {
          ctx.beginPath()
          ctx.lineTo(convertX(ctx, x), convertY(ctx, 0.3))
          ctx.lineTo(convertX(ctx, x), convertY(ctx, -0.3))
          ctx.stroke()

          // the text
          const text = x < 0 ? '-' + i : i
          ctx.font = `${textSize}px ${textFont}`
          ctx.fillStyle = textColor
          const measure = ctx.measureText(text)
          ctx.fillText(text, convertX(ctx, x) - measure.width / 2, convertY(ctx, -0.3) + 25)
        }
        // right 
        draw(i)
        // left
        draw(i * -1)
      }
    })

    // draws the unit markers in the y axis
    Array.from(Array(canvas.height / unitInPixels / 2).keys()).forEach(i => {
      if (i != 0) {
        const draw = function (x) {
          ctx.beginPath()
          ctx.lineTo(convertX(ctx, -0.3), convertY(ctx, x))
          ctx.lineTo(convertX(ctx, 0.3), convertY(ctx, x))
          ctx.stroke()

          // the text
          const text = x < 0 ? '-' + i : i
          ctx.font = `${textSize}px ${textFont}`
          ctx.fillStyle = textColor
          ctx.fillText(text, convertX(ctx, 0.3) + 10, convertY(ctx, x) + 10)
        }
        // top 
        draw(i)
        // bottom
        draw(i * -1)
      }
    })

    points.forEach(p1 => {
      // converts the line coords
      const realP1 = new Point(convertX(ctx, p1.x), convertY(ctx, p1.y))

      points.forEach(p2 => {
        const realP2 = new Point(convertX(ctx, p2.x), convertY(ctx, p2.y))

        // draws the line
        ctx.strokeStyle = lineColor
        ctx.beginPath()
        ctx.lineTo(realP1.x, realP1.y)
        ctx.lineTo(realP2.x, realP2.y)
        ctx.stroke()
      })
    })

    // draws the points
    ctx.fillStyle = dotColor
    // the dots aren't a circle, performance first! =3

    points.forEach(p => {
      ctx.fillRect(convertX(ctx, p.x) - dotSize / 2, convertY(ctx, p.y) - dotSize / 2, dotSize, dotSize)
    })

    // creates the image
    const attachment = new Discord.Attachment(canvas.toBuffer(), 'line.png')

    // send it
    context.send(context.author.toString(), attachment)
  }
}

module.exports = new LineCommand()
