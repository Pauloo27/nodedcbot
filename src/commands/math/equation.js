const { Command, Parameter, DefaultTypes } = require('../../command/command_api')

class EquationCommand extends Command {
  constructor() {
    super(
      'eq',
      'Resolve uma equação do segundo grau',
      '2x² + 5x + 3 = 0',
      [
        new Parameter('equação', 'equação a ser resolvida', DefaultTypes.text, true,
          function (arg, context) {
            arg = context.rawArgs
            return arg.match(/(-)?[0-9]+(.[0-9]+)?x² [+|-] [0-9]+(.[0-9]+)?x [+|-] [0-9]+(.[0-9]+)?/g)
          }
        )
      ],
      ['equacao', 'equação', 'solve']
    )
  }

  execute(context, eq) {
    // 2x² + 5x + 3 = 0
    eq = eq.split('=')[0]

    /*
    0 2x²
    1 + 
    2 5x 
    3 + 
    4 3
    */
    const members = eq.split(' ')

    const rawA = members[0]
    const rawB = members[1] + members[2]
    const rawC = members[3] + members[4]

    const a = parseFloat(rawA.match(/(-)?[0-9]+(.[0-9]+)?/g))
    const b = parseFloat(rawB.match(/(-)?[0-9]+(.[0-9]+)?/g))
    const c = parseFloat(rawC.match(/(-)?[0-9]+(.[0-9]+)?/g))

    // x1 = (-b + sqrt(b² - 4*a*c)) / 2*a
    // x2 = (-b - sqrt(b² - 4*a*c)) / 2*a

    const delta = Math.sqrt(b ** 2 - 4 * a * c)

    if (delta < 0) {
      context.error('Δ menor que 0! Não foi possível calcular!')
      return;
    }

    const x1 = (-b + delta) / 2 * a
    let x2 = x1
    if (delta != 0)
      x2 = (-b - delta) / 2 * a

    context.success(`${eq}\nΔ = ${delta}\nx' = ${x1}\nx" = ${x2}`)
  }
}

module.exports = new EquationCommand()
