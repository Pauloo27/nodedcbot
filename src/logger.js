const chalk = require('chalk')
const EventEmmiter = require('events')

const levelType = {
  'INFO': 'white',
  'COMMAND': 'blue',
  'WARNING': 'yellow',
  'DEBUG': 'green',
  'ERROR': 'red'
}

class Logger extends EventEmmiter {
  
  info(message) {
    this.log('info', message)
  }

  command(message) {
    this.log('command', message)
  }


  warning(message) {
    this.log('warning', message)
  }

  debug(message) {
    this.log('debug', message)
  }

  error(message) {
    this.log('error', message)
  }

  log(level, message) {
    level = level.toUpperCase()
    
    if(message == null) message = ''

    let levelColor = levelType[level in levelType ? level : 'info']
    this.emit(`log`, message, level)
    console.log(chalk.bold(chalk.keyword(levelColor)(`[${level}]`)), message)
  }

}

module.exports = Logger
