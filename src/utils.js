module.exports.copyDict = function(obj) {
  var clone = {}
  for (let property in obj) clone[property] = obj[property]
  return clone
}

module.exports.copyArray = function(arr) {
  var clone = []
  arr.forEach((obj) => clone.push(obj))
  return clone
}
