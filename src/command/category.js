class Category {
  constructor(name, description, defaultPermission = null) {
    this.name = name
    this.description = description
    this.defaultPermission = defaultPermission
  }
}

module.exports = Category
