const Type = require('./type')

const regexValitador = function (pattern, str) { return str.match(pattern) == str }

class SimpleType extends Type {
  constructor(name, description, validator, parser) {
    super(name, description)
    this.validator = validator
    this.parser = parser
  }
}

class String extends SimpleType {
  constructor() {
    super(
      'string',
      'texto sem espaço',
      function (arg, context) { return true },
      function (arg, context) { return arg }
    )
  }
}

class Integer extends SimpleType {
  constructor() {
    super(
      'int',
      'número inteiro',
      function (arg, context) { return !isNaN(arg) },
      function (arg, context) { return parseInt(arg) }
    )
  }
}

class Float extends SimpleType {
  constructor() {
    super(
      'float',
      'número decimal',
      function (arg, context) { return !isNaN(arg) },
      function (arg, context) { return parseFloat(arg) }
    )
  }
}

class Member extends Type {
  constructor() {
    super('member', 'menção à um membro')
  }

  validator(arg, context) {
    const ids = arg.match(/[0-9]{7,}/g)
    if (ids == null) return false
    const id = ids[0]
    return regexValitador(/<@(!)?[0-9]{7,}>/g, arg) && context.msg.mentions.members.has(id)
  }
  parser(arg, context) {
    const id = arg.match(/[0-9]{7,}/g)[0]
    return context.msg.mentions.members.get(id)
  }
}

class User extends Type {
  constructor() {
    super('user', 'menção à um usuário')
  }

  validator(arg, context) {
    const ids = arg.match(/[0-9]{7,}/g)
    if (ids == null) return false
    const id = ids[0]
    return regexValitador(/<@(!)?[0-9]{7,}>/g, arg) && context.msg.mentions.members.has(id)
  }
  parser(arg, context) {
    const id = arg.match(/[0-9]{7,}/g)[0]
    return context.msg.mentions.members.get(id).user
  }
}

class Enum extends Type {
  constructor(...values) {
    values = values.map(v => v.toLowerCase())
    super('enum', values.join(', '))
    this.values = values
  }

  validator(arg, context) { return this.values.includes(arg.toLowerCase()) }

  parser(arg, context) { return arg }
}

class Text extends Type {
  constructor() {
    super('text', 'texto com espaço')
  }

  validator(arg, context) { return context.count <= (context.args.length - 1) }

  parser(arg, context) {
    const utils = require('../utils')
    var args = utils.copyArray(context.args)
    args.splice(0, context.count)
    return args.join(' ')
  }
}

module.exports.string = new String()
module.exports.int = new Integer()
module.exports.float = new Float()
module.exports.member = new Member()
module.exports.user = new User()
module.exports.enum = Enum
module.exports.text = new Text()
