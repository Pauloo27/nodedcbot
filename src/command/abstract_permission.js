class AbstractPermission {
    constructor(name, description, errorMessage = 'Sem permissão') {
        this.name = name
        this.description = description
        this.errorMessage = errorMessage
    }

    hasPermission(context) { }
}

module.exports = AbstractPermission
