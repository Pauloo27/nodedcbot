var commands = {}
var categories = {}

const { logger } = require('../app')

module.exports.prefix = '!'

module.exports.run = function(msg) {
  const content = msg.content.replace(/\s+/g,' ')
  var prefix = module.exports.prefix
  if(!content.startsWith(prefix)) return false
  var commandName = content.split(' ')[0].substring(prefix.length).toLowerCase()
  if(!content.includes(' ')) var rawArgs = null
  else var rawArgs = content.substring(prefix.length + commandName.length + 1)

  if(commandName in commands) {
    const command = commands[commandName]
    
    var context = {
      msg: msg,
      author: msg.author,
      member: msg.member,
      command: command,
      commandsMap: commands,
      rawContent: msg.content,
      content: content,
      commandName: commandName,
      prefix: prefix,
      args: rawArgs == null ? null : rawArgs.split(' '),
      rawArgs: rawArgs,
      send: function (obj, more) { msg.channel.send(obj, more) },
      reply: function (text) { msg.reply(text) },
      error: function (text) { msg.reply(':x: ' + text) },
      success: function (text) { msg.reply(':white_check_mark: ' + text) }
    }

    if(!command.hasPermission(context)) {
      context.error('Sem permissão. Permissão necessária: ' + command.permission.description + ".")
      logger.command(`${context.author.tag} (${context.author.id}) has no permission to command '${context.command.name.toUpperCase()}' > ${context.msg}`)
    } else if(command.isValid(context)) {
      var parsedArgs = command.parse(context)
      
      if(parsedArgs == null) command.execute(context)
      else command.execute(context, ...parsedArgs)

      logger.command(`${context.author.tag} (${context.author.id}) issued command '${context.command.name.toUpperCase()}' > ${context.msg}`)

    } else {
      msg.channel.send(command.usageEmbed(context))
      logger.command(`${context.author.tag} (${context.author.id}) wrong used command '${context.command.name.toUpperCase()}' > ${context.msg}`)
    }
  } else {
    return false
  }
}

module.exports.register = function(command) {
  commands[command.name.toLowerCase()] = command
  const category = command.category
  if(!(category.name in categories)) {
    categories[category.name] = category
  }

  if(command.aliases == null || command.aliases.length == 0) return

  command.aliases.forEach((alias) => {
    commands[alias.toLowerCase()] = command
  })
}

module.exports.clear = function() {
  commands = {}
  categories = {}
}
