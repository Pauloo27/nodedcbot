class Parameter {
  constructor(name, description, type, required, validator = null, vararg = false) {
    this.name = name
    this.description = description
    this.type = type
    this.required = required
    this.validator = validator
    this.vararg = vararg
  }

  isValid(arg, context) {
    return this.type.validator(arg, context) && (this.validator == null || this.validator(arg, context))
  }

  parse(arg, context) {
    return this.type.parser(arg, context)
  }

}

module.exports = Parameter
