const { RichEmbed } = require('discord.js')
const { copyDict } = require('../utils')

class Command {
  constructor(name, description, examples, parameters = null, aliases = null, permission = null, preValidator = null) {
    this.name = name
    this.aliases = aliases
    this.description = description
    this.examples = examples
    this.parameters = parameters
    this.permission = permission
    this.preValidator = preValidator
  }

  hasPermission(context) {
    if (this.permission == null)
      return true

    return this.permission.hasPermission(context)
  }

  execute(context) { }

  isValid(context) {
    const args = context.args

    if (this.preValidator != null && !this.preValidator(context))
      return false


    if (!this.parameters || this.parameters.length == 0) return true

    let count = 0

    var valid = true
    this.parameters.forEach(parameter => {
      if (!valid)
        return false;

      var localContext = copyDict(context)
      localContext.count = count

      if (args && args.length > count) {
        if (!parameter.isValid(args[count], localContext)) valid = false
      }
      else if (parameter.required) valid = false

      count++

      if (valid && args && args.length > count && this.parameters.length <= count && parameter.vararg) {
        Array.from(Array(args.length - this.parameters.length).keys()).forEach(i => {
          var localContext = copyDict(context)
          localContext.count = count

          if (valid && !parameter.isValid(args[count + i], localContext)) valid = false
        })
      }
    })

    return valid;
  }

  parse(context) {
    if (context.args != null && this.isValid(context)) {
      const parsedArgs = []
      const args = context.args
      let count = 0
      if (!this.parameters || this.parameters.length == 0) return null

      this.parameters.forEach(parameter => {
        var localContext = copyDict(context)
        localContext.count = count
        if (args.length > count) {
          parsedArgs.push(parameter.parse(args[count], localContext))
        }
        count++

        if (args.length > count && this.parameters.length <= count && parameter.vararg) {
          Array.from(Array(args.length - this.parameters.length).keys()).forEach(i => {
            var localContext = copyDict(context)
            localContext.count = count
            parsedArgs.push(parameter.parse(args[count + i], localContext))
          })
        }

      })
      return parsedArgs
    } else {
      return null;
    }
  }

  usage(context) {
    let text = context.prefix + context.commandName + ' '
    this.parameters.forEach((parameter) => {
      text += (parameter.required ? `**[<${parameter.name}>]**` : `*<${parameter.name}>*`) + (parameter.vararg ? `...` : ``) + ' '
    })
    return text.trim()
  }

  usageEmbed(context, title = ':x: Uso inválido') {
    var examplesFormatted = ''
    if (typeof (this.examples) == 'string') {
      examplesFormatted = `${context.prefix}${context.commandName} ${this.examples}`
    }
    else {
      this.examples.forEach(example => {
        examplesFormatted += `${context.prefix}${context.commandName} ${example}\n`
      })
    }

    var embed = new RichEmbed()
      .setColor('#ff0000')
      .setTitle(title)
      .setDescription(`Comando **${this.name.toUpperCase()}**`)
      .addField('Descrição', `*${this.description}*`, false)
      .addField('Uso', this.usage(context), false)
      .addField('Exemplos', examplesFormatted.trim(), false)
      .setTimestamp()
      .setFooter(context.msg.author.tag, context.msg.author.avatarURL);
    if (this.aliases != null && this.aliases.length != 0) {
      embed.addField('Alternativas', this.aliases.join(', '), false)
    }
    return embed;
  }
}

module.exports = Command
