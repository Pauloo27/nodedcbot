const AbstractPermission = require('./abstract_permission')
const { ownerId } = require('../../config')
const app = require('../app')

class Owner extends AbstractPermission {
  constructor() {
    super('owner', 'ser o dono do Bot')
  }

  hasPermission(context) {
    return context.author.id === ownerId || context.author.id === app.client.user.id
  }
}

class ByRole extends AbstractPermission {
  constructor(name, roleId) {
    super(name, 'ter o cargo ' + name)
    this.roleId = roleId
  }

  hasPermission(context) {
    return context.member.roles.has(this.roleId)
  }
}

class ByDiscordPermission extends AbstractPermission {
  constructor(name, permission) {
    super(name, name + ' (' + permission + ')')
    this.permission = permission
  }

  hasPermission(context) {
    return context.member.hasPermission(this.permission)
  }
}

module.exports.Owner = Owner
module.exports.ByRole = ByRole
module.exports.ByDiscordPermission = ByDiscordPermission
